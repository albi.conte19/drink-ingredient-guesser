// import { sortByAlphabeticalOrder } from './helpers';
import {
    getDrinkFromFirstLetter,
    getIngredientsFromDrink,
    getEveryDrink,
} from './drink';
import { myFecth } from './myFetch';

async function getAllIngredients() {
    const raw = await myFecth(
        'https://www.thecocktaildb.com/api/json/v1/1/list.php?i=list'
    );
    // console.log(raw.drinks);
    const ingredients = raw.drinks.map(
        (ingredient) => ingredient.strIngredient1
    );
    // console.log(ingredients);
    return ingredients.sort();
}

function createItem(ingredient, drinkIngredients) {
    const li = document.createElement('li');
    const p = document.createElement('p');
    const img = document.createElement('img');

    img.src = `https://www.thecocktaildb.com/images/ingredients/${ingredient}-Small.png`;
    p.innerHTML = ingredient;
    li.appendChild(p);
    li.appendChild(img);

    li.addEventListener('click', () => {
        if (drinkIngredients.includes(ingredient)) {
            li.classList.toggle('correct');
        } else {
            li.classList.toggle('wrong');
        }
    });
    return li;
}

function getEveryIngredient() {
    return new Promise(async (resolve, reject) => {
        const allDrinks = await getEveryDrink();
        let i = 0;
        const allIngredients = [];
        allDrinks.forEach(async (drink) => {
            const ingredients = await getIngredientsFromDrink(drink);
            ingredients.forEach((ingredient) => {
                let ingredientFoundInArray = false;
                allIngredients.forEach((ingredientInArray) => {
                    if (
                        ingredientInArray.toLowerCase() ===
                        ingredient.toLowerCase()
                    ) {
                        ingredientFoundInArray = true;
                    }
                });
                !ingredientFoundInArray && allIngredients.push(ingredient);
            });
            if (i === allDrinks.length - 1) {
                resolve(allIngredients.sort());
            }
            i++;
        });
    });
}

export { getAllIngredients, createItem, getEveryIngredient };
