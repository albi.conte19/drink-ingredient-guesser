import { myFecth } from './myFetch';

async function getDrink() {
    return await myFecth(
        'https://www.thecocktaildb.com/api/json/v1/1/random.php'
    ).then((res) => res.drinks[0]);
}

async function getIngredientsFromDrink(drink) {
    const ingredientsOfDrink = [];
    for (const key in drink) {
        if (Object.hasOwnProperty.call(drink, key)) {
            const element = drink[key];
            if (key.includes('strIngredient') && element !== null) {
                ingredientsOfDrink.push(element);
            }
        }
    }
    return ingredientsOfDrink;
}

async function getDrinkFromFirstLetter(letter) {
    return await myFecth(
        'https://www.thecocktaildb.com/api/json/v1/1/search.php?f=' + letter
    ).then((res) => res.drinks);
}
function getEveryDrink() {
    const letters = [
        'a',
        'b',
        'c',
        'd',
        'e',
        'f',
        'g',
        'h',
        'i',
        'j',
        'k',
        'l',
        'm',
        'n',
        'o',
        'p',
        'q',
        'r',
        's',
        't',
        'u',
        'v',
        'w',
        'x',
        'y',
        'z',
    ];
    const allDrinks = [];
    let i = 0;
    return new Promise((resolve, reject) => {
        letters.forEach((letter) => {
            getDrinkFromFirstLetter(letter).then((drinks) => {
                // console.log(letter);
                if (drinks && drinks.length > 0) {
                    allDrinks.push(...drinks);
                }
                if (i === letters.length - 1) {
                    resolve(allDrinks);
                }
                i++;
            });
        });
    });
}

export {
    getDrink,
    getIngredientsFromDrink,
    getDrinkFromFirstLetter,
    getEveryDrink,
};
