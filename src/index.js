import './styles/style.scss';
// import { myFecth } from './modules/myFetch';
import { getDrink, getIngredientsFromDrink } from './modules/drink';
import {
    getAllIngredients,
    createItem,
    getEveryIngredient,
} from './modules/ingredients';

const root = document.getElementById('root');
const ingredientsList = document.getElementById('ingredientsList');
const drinkElement = document.getElementById('drink');

async function start() {
    drinkElement.innerHTML = '';
    const drink = { ...(await getDrink()) };
    console.log(drink);

    const title = document.createElement('p');
    const drinkThumb = document.createElement('img');

    title.innerHTML = drink.strDrink;
    drinkThumb.src = drink.strDrinkThumb;

    drinkElement.appendChild(title);
    drinkElement.appendChild(drinkThumb);

    const ingredientsOfDrink = [...(await getIngredientsFromDrink(drink))];
    console.log(ingredientsOfDrink);

    const allIngredients = [...(await getEveryIngredient())];
    populateList(allIngredients, ingredientsOfDrink);

    document.getElementById('search').addEventListener('input', (e) => {
        const search = e.target.value.toLowerCase();
        populateList(
            allIngredients.filter((i) => i.toLowerCase().includes(search)),
            ingredientsOfDrink
        );
    });
}

function populateList(ingredients, ingredientsOfDrink) {
    ingredientsList.innerHTML = '';
    ingredients.forEach((ingredient) => {
        const item = createItem(ingredient, ingredientsOfDrink);
        ingredientsList.appendChild(item);
    });
}

document.getElementById('reset').addEventListener('click', () => {
    start();
});

document.addEventListener('readystatechange', () => {
    start();
});
